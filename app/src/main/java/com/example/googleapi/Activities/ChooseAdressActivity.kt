package com.example.googleapi.Activities

import android.graphics.PorterDuff
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.googleapi.*
import com.example.googleapi.Adapters.Adapter
import com.example.googleapi.Dataloaders.ApiRequest
import com.example.googleapi.Interfaces.CallbackApi
import com.example.googleapi.Models.MAinViewModel
import com.example.googleapi.Models.MyModel
import com.example.googleapi.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.main_toolbar.*
import org.json.JSONObject

class ChooseAdressActivity : AppCompatActivity() {
    lateinit var adapter: Adapter
    var itemslist= mutableListOf<MyModel>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val model=ViewModelProvider(this)[MAinViewModel::class.java]
        val binding:ActivityMainBinding= DataBindingUtil.setContentView(this,
            R.layout.activity_main
        )
        binding.viewmodel=model
        binding.lifecycleOwner=this
        addadapter()
        init(model)

    }
    private fun init(model: MAinViewModel){
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        toolbar.navigationIcon!!.setColorFilter(resources.getColor(R.color.white), PorterDuff.Mode.SRC_ATOP)
        tootlbartitle.text="Area of Interest"
        done.text="Add"
        val map= mutableMapOf<String,String>()
        model.input.observe(this, Observer {
            if(it.isEmpty()) itemslist.clear()
            map["input"]=it
            map["key"]="AIzaSyBADWUmhO9XNVF_-qSZR6RQWcoHfSpAr6E"
            ApiRequest.getRequest(
                "autocomplete",
                map,
                object : CallbackApi {
                    override fun onResponse(value: String?) {
                        itemslist.clear()
                        val jsonobj = JSONObject(value)
                        if (jsonobj.has("predictions")) {
                            val jsonarry = jsonobj.getJSONArray("predictions")
                            (0 until jsonarry.length()).forEach {
                                val jsonobject = jsonarry.getJSONObject(it)
                                itemslist.add(
                                    MyModel(
                                        jsonobject.getString("description"),
                                        jsonobject.getString("place_id")
                                    )
                                )
                                addadapter()
                            }
                        }
                    }

                    override fun onFailure(value: String?) {
                        Toast.makeText(this@ChooseAdressActivity, value, Toast.LENGTH_LONG).show()
                    }
                },
                this
            )
        })
        adapter.notifyDataSetChanged()
    }
    private fun addadapter(){
        adapter= Adapter(itemslist)
        recyclerview.layoutManager=LinearLayoutManager(this)
        recyclerview.adapter=adapter
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId==android.R.id.home){
            super.onBackPressed()
        }
        return true
    }
}

