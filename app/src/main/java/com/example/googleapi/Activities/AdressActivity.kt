package com.example.googleapi.Activities

import android.content.Intent
import android.graphics.PorterDuff
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.example.googleapi.R
import kotlinx.android.synthetic.main.activity_adress.*
import kotlinx.android.synthetic.main.custom_chooser.view.*
import kotlinx.android.synthetic.main.main_toolbar.*

class AdressActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_adress)
        init()
    }
    private fun init(){
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        toolbar.navigationIcon!!.setColorFilter(resources.getColor(R.color.white), PorterDuff.Mode.SRC_ATOP)
        done.text="Finalize"

        street.tiitlechoser.text="Street Adress"
        suite.tiitlechoser.text="Apt,Suite . (optional)"
        zip.tiitlechoser.text="ZIP Code"

        country.setOnClickListener(){
            val intet= Intent(this,
                ChooseAdressActivity::class.java)
            startActivity(intet)
        }
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId==android.R.id.home){
            super.onBackPressed()
        }
        return true
    }

}
