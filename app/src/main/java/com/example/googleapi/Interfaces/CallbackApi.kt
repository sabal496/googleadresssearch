package com.example.googleapi.Interfaces

interface CallbackApi {
    fun onResponse(value:String?)
    fun onFailure(value:String?)
}